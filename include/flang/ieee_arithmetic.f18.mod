﻿!mod$ v1 sum:488ffacb68d31a72
module ieee_arithmetic
use __fortran_builtins,only:ieee_is_nan=>__builtin_ieee_is_nan
use __fortran_builtins,only:ieee_is_normal=>__builtin_ieee_is_normal
use __fortran_builtins,only:ieee_is_negative=>__builtin_ieee_is_negative
use __fortran_builtins,only:ieee_next_after=>__builtin_ieee_next_after
use __fortran_builtins,only:ieee_next_down=>__builtin_ieee_next_down
use __fortran_builtins,only:ieee_next_up=>__builtin_ieee_next_up
use __fortran_builtins,only:ieee_scalb=>scale
use __fortran_builtins,only:ieee_selected_real_kind=>__builtin_ieee_selected_real_kind
use __fortran_builtins,only:ieee_support_datatype=>__builtin_ieee_support_datatype
use __fortran_builtins,only:ieee_support_denormal=>__builtin_ieee_support_denormal
use __fortran_builtins,only:ieee_support_divide=>__builtin_ieee_support_divide
use __fortran_builtins,only:ieee_support_inf=>__builtin_ieee_support_inf
use __fortran_builtins,only:ieee_support_io=>__builtin_ieee_support_io
use __fortran_builtins,only:ieee_support_nan=>__builtin_ieee_support_nan
use __fortran_builtins,only:ieee_support_sqrt=>__builtin_ieee_support_sqrt
use __fortran_builtins,only:ieee_support_standard=>__builtin_ieee_support_standard
use __fortran_builtins,only:ieee_support_subnormal=>__builtin_ieee_support_subnormal
use __fortran_builtins,only:ieee_support_underflow_control=>__builtin_ieee_support_underflow_control
type::ieee_class_type
integer(1),private::which=0_1
end type
type(ieee_class_type),parameter::ieee_signaling_nan=ieee_class_type(which=1_1)
type(ieee_class_type),parameter::ieee_quiet_nan=ieee_class_type(which=2_1)
type(ieee_class_type),parameter::ieee_negative_inf=ieee_class_type(which=3_1)
type(ieee_class_type),parameter::ieee_negative_normal=ieee_class_type(which=4_1)
type(ieee_class_type),parameter::ieee_negative_denormal=ieee_class_type(which=5_1)
type(ieee_class_type),parameter::ieee_negative_zero=ieee_class_type(which=6_1)
type(ieee_class_type),parameter::ieee_positive_zero=ieee_class_type(which=7_1)
type(ieee_class_type),parameter::ieee_positive_subnormal=ieee_class_type(which=8_1)
type(ieee_class_type),parameter::ieee_positive_normal=ieee_class_type(which=9_1)
type(ieee_class_type),parameter::ieee_positive_inf=ieee_class_type(which=10_1)
type(ieee_class_type),parameter::ieee_other_value=ieee_class_type(which=11_1)
type(ieee_class_type),parameter::ieee_negative_subnormal=ieee_class_type(which=5_1)
type(ieee_class_type),parameter::ieee_positive_denormal=ieee_class_type(which=5_1)
type::ieee_round_type
integer(1),private::mode=0_1
end type
type(ieee_round_type),parameter::ieee_nearest=ieee_round_type(mode=1_1)
type(ieee_round_type),parameter::ieee_to_zero=ieee_round_type(mode=2_1)
type(ieee_round_type),parameter::ieee_up=ieee_round_type(mode=3_1)
type(ieee_round_type),parameter::ieee_down=ieee_round_type(mode=4_1)
type(ieee_round_type),parameter::ieee_away=ieee_round_type(mode=5_1)
type(ieee_round_type),parameter::ieee_other=ieee_round_type(mode=6_1)
interface operator(==)
procedure::class_eq
procedure::round_eq
end interface
interface operator(/=)
procedure::class_ne
procedure::round_ne
end interface
interface ieee_class
procedure::ieee_class_a2
procedure::ieee_class_a3
procedure::ieee_class_a4
procedure::ieee_class_a8
procedure::ieee_class_a10
procedure::ieee_class_a16
end interface
interface ieee_copy_sign
procedure::ieee_copy_sign_a2
procedure::ieee_copy_sign_a3
procedure::ieee_copy_sign_a4
procedure::ieee_copy_sign_a8
procedure::ieee_copy_sign_a10
procedure::ieee_copy_sign_a16
end interface
interface ieee_is_finite
procedure::ieee_is_finite_a2
procedure::ieee_is_finite_a3
procedure::ieee_is_finite_a4
procedure::ieee_is_finite_a8
procedure::ieee_is_finite_a10
procedure::ieee_is_finite_a16
end interface
interface ieee_rem
procedure::ieee_rem_a2_a2
procedure::ieee_rem_a2_a3
procedure::ieee_rem_a2_a4
procedure::ieee_rem_a2_a8
procedure::ieee_rem_a2_a10
procedure::ieee_rem_a2_a16
procedure::ieee_rem_a3_a2
procedure::ieee_rem_a3_a3
procedure::ieee_rem_a3_a4
procedure::ieee_rem_a3_a8
procedure::ieee_rem_a3_a10
procedure::ieee_rem_a3_a16
procedure::ieee_rem_a4_a2
procedure::ieee_rem_a4_a3
procedure::ieee_rem_a4_a4
procedure::ieee_rem_a4_a8
procedure::ieee_rem_a4_a10
procedure::ieee_rem_a4_a16
procedure::ieee_rem_a8_a2
procedure::ieee_rem_a8_a3
procedure::ieee_rem_a8_a4
procedure::ieee_rem_a8_a8
procedure::ieee_rem_a8_a10
procedure::ieee_rem_a8_a16
procedure::ieee_rem_a10_a2
procedure::ieee_rem_a10_a3
procedure::ieee_rem_a10_a4
procedure::ieee_rem_a10_a8
procedure::ieee_rem_a10_a10
procedure::ieee_rem_a10_a16
procedure::ieee_rem_a16_a2
procedure::ieee_rem_a16_a3
procedure::ieee_rem_a16_a4
procedure::ieee_rem_a16_a8
procedure::ieee_rem_a16_a10
procedure::ieee_rem_a16_a16
end interface
interface ieee_support_rounding
procedure::ieee_support_rounding_
procedure::ieee_support_rounding_2
procedure::ieee_support_rounding_3
procedure::ieee_support_rounding_4
procedure::ieee_support_rounding_8
procedure::ieee_support_rounding_10
procedure::ieee_support_rounding_16
end interface
private::class_eq
private::class_ne
private::round_eq
private::round_ne
private::classify
private::ieee_class_a2
private::ieee_class_a3
private::ieee_class_a4
private::ieee_class_a8
private::ieee_class_a10
private::ieee_class_a16
private::ieee_copy_sign_a2
private::ieee_copy_sign_a3
private::ieee_copy_sign_a4
private::ieee_copy_sign_a8
private::ieee_copy_sign_a10
private::ieee_copy_sign_a16
private::ieee_is_finite_a2
private::ieee_is_finite_a3
private::ieee_is_finite_a4
private::ieee_is_finite_a8
private::ieee_is_finite_a10
private::ieee_is_finite_a16
private::ieee_rem_a2_a2
private::ieee_rem_a2_a3
private::ieee_rem_a2_a4
private::ieee_rem_a2_a8
private::ieee_rem_a2_a10
private::ieee_rem_a2_a16
private::ieee_rem_a3_a2
private::ieee_rem_a3_a3
private::ieee_rem_a3_a4
private::ieee_rem_a3_a8
private::ieee_rem_a3_a10
private::ieee_rem_a3_a16
private::ieee_rem_a4_a2
private::ieee_rem_a4_a3
private::ieee_rem_a4_a4
private::ieee_rem_a4_a8
private::ieee_rem_a4_a10
private::ieee_rem_a4_a16
private::ieee_rem_a8_a2
private::ieee_rem_a8_a3
private::ieee_rem_a8_a4
private::ieee_rem_a8_a8
private::ieee_rem_a8_a10
private::ieee_rem_a8_a16
private::ieee_rem_a10_a2
private::ieee_rem_a10_a3
private::ieee_rem_a10_a4
private::ieee_rem_a10_a8
private::ieee_rem_a10_a10
private::ieee_rem_a10_a16
private::ieee_rem_a16_a2
private::ieee_rem_a16_a3
private::ieee_rem_a16_a4
private::ieee_rem_a16_a8
private::ieee_rem_a16_a10
private::ieee_rem_a16_a16
private::ieee_support_rounding_
private::ieee_support_rounding_2
private::ieee_support_rounding_3
private::ieee_support_rounding_4
private::ieee_support_rounding_8
private::ieee_support_rounding_10
private::ieee_support_rounding_16
contains
elemental function class_eq(x,y)
type(ieee_class_type),intent(in)::x
type(ieee_class_type),intent(in)::y
logical(4)::class_eq
end
elemental function class_ne(x,y)
type(ieee_class_type),intent(in)::x
type(ieee_class_type),intent(in)::y
logical(4)::class_ne
end
elemental function round_eq(x,y)
type(ieee_round_type),intent(in)::x
type(ieee_round_type),intent(in)::y
logical(4)::round_eq
end
elemental function round_ne(x,y)
type(ieee_round_type),intent(in)::x
type(ieee_round_type),intent(in)::y
logical(4)::round_ne
end
elemental function classify(expo,maxexpo,negative,significandnz,quietbit)
integer(4),intent(in)::expo
integer(4),intent(in)::maxexpo
logical(4),intent(in)::negative
logical(4),intent(in)::significandnz
logical(4),intent(in)::quietbit
type(ieee_class_type)::classify
end
elemental function ieee_class_a2(x)
real(2),intent(in)::x
type(ieee_class_type)::ieee_class_a2
end
elemental function ieee_class_a3(x)
real(3),intent(in)::x
type(ieee_class_type)::ieee_class_a3
end
elemental function ieee_class_a4(x)
real(4),intent(in)::x
type(ieee_class_type)::ieee_class_a4
end
elemental function ieee_class_a8(x)
real(8),intent(in)::x
type(ieee_class_type)::ieee_class_a8
end
elemental function ieee_class_a10(x)
real(10),intent(in)::x
type(ieee_class_type)::ieee_class_a10
end
elemental function ieee_class_a16(x)
real(16),intent(in)::x
type(ieee_class_type)::ieee_class_a16
end
elemental function ieee_copy_sign_a2(x,y)
real(2),intent(in)::x
real(2),intent(in)::y
real(2)::ieee_copy_sign_a2
end
elemental function ieee_copy_sign_a3(x,y)
real(3),intent(in)::x
real(3),intent(in)::y
real(3)::ieee_copy_sign_a3
end
elemental function ieee_copy_sign_a4(x,y)
real(4),intent(in)::x
real(4),intent(in)::y
real(4)::ieee_copy_sign_a4
end
elemental function ieee_copy_sign_a8(x,y)
real(8),intent(in)::x
real(8),intent(in)::y
real(8)::ieee_copy_sign_a8
end
elemental function ieee_copy_sign_a10(x,y)
real(10),intent(in)::x
real(10),intent(in)::y
real(10)::ieee_copy_sign_a10
end
elemental function ieee_copy_sign_a16(x,y)
real(16),intent(in)::x
real(16),intent(in)::y
real(16)::ieee_copy_sign_a16
end
elemental function ieee_is_finite_a2(x) result(res)
real(2),intent(in)::x
logical(4)::res
end
elemental function ieee_is_finite_a3(x) result(res)
real(3),intent(in)::x
logical(4)::res
end
elemental function ieee_is_finite_a4(x) result(res)
real(4),intent(in)::x
logical(4)::res
end
elemental function ieee_is_finite_a8(x) result(res)
real(8),intent(in)::x
logical(4)::res
end
elemental function ieee_is_finite_a10(x) result(res)
real(10),intent(in)::x
logical(4)::res
end
elemental function ieee_is_finite_a16(x) result(res)
real(16),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a2(x) result(res)
real(2),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a3(x) result(res)
real(3),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a4(x) result(res)
real(4),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a8(x) result(res)
real(8),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a10(x) result(res)
real(10),intent(in)::x
logical(4)::res
end
elemental function ieee_is_negative_a16(x) result(res)
real(16),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a2(x) result(res)
real(2),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a3(x) result(res)
real(3),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a4(x) result(res)
real(4),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a8(x) result(res)
real(8),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a10(x) result(res)
real(10),intent(in)::x
logical(4)::res
end
elemental function ieee_is_normal_a16(x) result(res)
real(16),intent(in)::x
logical(4)::res
end
elemental function ieee_rem_a2_a2(x,y) result(res)
real(2),intent(in)::x
real(2),intent(in)::y
real(2)::res
end
elemental function ieee_rem_a2_a3(x,y) result(res)
real(2),intent(in)::x
real(3),intent(in)::y
real(3)::res
end
elemental function ieee_rem_a2_a4(x,y) result(res)
real(2),intent(in)::x
real(4),intent(in)::y
real(4)::res
end
elemental function ieee_rem_a2_a8(x,y) result(res)
real(2),intent(in)::x
real(8),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a2_a10(x,y) result(res)
real(2),intent(in)::x
real(10),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a2_a16(x,y) result(res)
real(2),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a3_a2(x,y) result(res)
real(3),intent(in)::x
real(2),intent(in)::y
real(3)::res
end
elemental function ieee_rem_a3_a3(x,y) result(res)
real(3),intent(in)::x
real(3),intent(in)::y
real(3)::res
end
elemental function ieee_rem_a3_a4(x,y) result(res)
real(3),intent(in)::x
real(4),intent(in)::y
real(4)::res
end
elemental function ieee_rem_a3_a8(x,y) result(res)
real(3),intent(in)::x
real(8),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a3_a10(x,y) result(res)
real(3),intent(in)::x
real(10),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a3_a16(x,y) result(res)
real(3),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a4_a2(x,y) result(res)
real(4),intent(in)::x
real(2),intent(in)::y
real(4)::res
end
elemental function ieee_rem_a4_a3(x,y) result(res)
real(4),intent(in)::x
real(3),intent(in)::y
real(4)::res
end
elemental function ieee_rem_a4_a4(x,y) result(res)
real(4),intent(in)::x
real(4),intent(in)::y
real(4)::res
end
elemental function ieee_rem_a4_a8(x,y) result(res)
real(4),intent(in)::x
real(8),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a4_a10(x,y) result(res)
real(4),intent(in)::x
real(10),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a4_a16(x,y) result(res)
real(4),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a8_a2(x,y) result(res)
real(8),intent(in)::x
real(2),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a8_a3(x,y) result(res)
real(8),intent(in)::x
real(3),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a8_a4(x,y) result(res)
real(8),intent(in)::x
real(4),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a8_a8(x,y) result(res)
real(8),intent(in)::x
real(8),intent(in)::y
real(8)::res
end
elemental function ieee_rem_a8_a10(x,y) result(res)
real(8),intent(in)::x
real(10),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a8_a16(x,y) result(res)
real(8),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a10_a2(x,y) result(res)
real(10),intent(in)::x
real(2),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a10_a3(x,y) result(res)
real(10),intent(in)::x
real(3),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a10_a4(x,y) result(res)
real(10),intent(in)::x
real(4),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a10_a8(x,y) result(res)
real(10),intent(in)::x
real(8),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a10_a10(x,y) result(res)
real(10),intent(in)::x
real(10),intent(in)::y
real(10)::res
end
elemental function ieee_rem_a10_a16(x,y) result(res)
real(10),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a2(x,y) result(res)
real(16),intent(in)::x
real(2),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a3(x,y) result(res)
real(16),intent(in)::x
real(3),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a4(x,y) result(res)
real(16),intent(in)::x
real(4),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a8(x,y) result(res)
real(16),intent(in)::x
real(8),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a10(x,y) result(res)
real(16),intent(in)::x
real(10),intent(in)::y
real(16)::res
end
elemental function ieee_rem_a16_a16(x,y) result(res)
real(16),intent(in)::x
real(16),intent(in)::y
real(16)::res
end
pure function ieee_support_rounding_(round_type)
type(ieee_round_type),intent(in)::round_type
logical(4)::ieee_support_rounding_
end
pure function ieee_support_rounding_2(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(2),intent(in)::x
logical(4)::ieee_support_rounding_2
end
pure function ieee_support_rounding_3(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(3),intent(in)::x
logical(4)::ieee_support_rounding_3
end
pure function ieee_support_rounding_4(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(4),intent(in)::x
logical(4)::ieee_support_rounding_4
end
pure function ieee_support_rounding_8(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(8),intent(in)::x
logical(4)::ieee_support_rounding_8
end
pure function ieee_support_rounding_10(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(10),intent(in)::x
logical(4)::ieee_support_rounding_10
end
pure function ieee_support_rounding_16(round_type,x)
type(ieee_round_type),intent(in)::round_type
real(16),intent(in)::x
logical(4)::ieee_support_rounding_16
end
end
