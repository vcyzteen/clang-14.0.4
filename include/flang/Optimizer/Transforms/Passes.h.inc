/* Autogenerated by mlir-tblgen; don't manually edit */
#ifdef GEN_PASS_CLASSES

//===----------------------------------------------------------------------===//
// AbstractResultOpt
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class AbstractResultOptBase : public ::mlir::OperationPass<mlir::FuncOp> {
public:
  using Base = AbstractResultOptBase;

  AbstractResultOptBase() : ::mlir::OperationPass<mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  AbstractResultOptBase(const AbstractResultOptBase &other) : ::mlir::OperationPass<mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("abstract-result-opt");
  }
  ::llvm::StringRef getArgument() const override { return "abstract-result-opt"; }

  ::llvm::StringRef getDescription() const override { return "Convert fir.array, fir.box and fir.rec function result to function argument"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("AbstractResultOpt");
  }
  ::llvm::StringRef getName() const override { return "AbstractResultOpt"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  registry.insert<mlir::StandardOpsDialect>();

  }

protected:
  ::mlir::Pass::Option<bool> passResultAsBox{*this, "abstract-result-as-box", ::llvm::cl::desc("Pass fir.array<T> result as fir.box<fir.array<T>> argument instead of fir.ref<fir.array<T>>."), ::llvm::cl::init(false)};
};

//===----------------------------------------------------------------------===//
// AffineDialectDemotion
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class AffineDialectDemotionBase : public ::mlir::OperationPass<::mlir::FuncOp> {
public:
  using Base = AffineDialectDemotionBase;

  AffineDialectDemotionBase() : ::mlir::OperationPass<::mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  AffineDialectDemotionBase(const AffineDialectDemotionBase &other) : ::mlir::OperationPass<::mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("demote-affine");
  }
  ::llvm::StringRef getArgument() const override { return "demote-affine"; }

  ::llvm::StringRef getDescription() const override { return "Converts `affine.{load,store}` back to fir operations"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("AffineDialectDemotion");
  }
  ::llvm::StringRef getName() const override { return "AffineDialectDemotion"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  registry.insert<mlir::StandardOpsDialect>();

  registry.insert<mlir::AffineDialect>();

  }

protected:
};

//===----------------------------------------------------------------------===//
// AffineDialectPromotion
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class AffineDialectPromotionBase : public ::mlir::OperationPass<::mlir::FuncOp> {
public:
  using Base = AffineDialectPromotionBase;

  AffineDialectPromotionBase() : ::mlir::OperationPass<::mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  AffineDialectPromotionBase(const AffineDialectPromotionBase &other) : ::mlir::OperationPass<::mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("promote-to-affine");
  }
  ::llvm::StringRef getArgument() const override { return "promote-to-affine"; }

  ::llvm::StringRef getDescription() const override { return "Promotes `fir.{do_loop,if}` to `affine.{for,if}`."; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("AffineDialectPromotion");
  }
  ::llvm::StringRef getName() const override { return "AffineDialectPromotion"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  registry.insert<mlir::StandardOpsDialect>();

  registry.insert<mlir::AffineDialect>();

  }

protected:
};

//===----------------------------------------------------------------------===//
// ArrayValueCopy
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class ArrayValueCopyBase : public ::mlir::OperationPass<::mlir::FuncOp> {
public:
  using Base = ArrayValueCopyBase;

  ArrayValueCopyBase() : ::mlir::OperationPass<::mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  ArrayValueCopyBase(const ArrayValueCopyBase &other) : ::mlir::OperationPass<::mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("array-value-copy");
  }
  ::llvm::StringRef getArgument() const override { return "array-value-copy"; }

  ::llvm::StringRef getDescription() const override { return "Convert array value operations to memory operations."; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("ArrayValueCopy");
  }
  ::llvm::StringRef getName() const override { return "ArrayValueCopy"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  }

protected:
};

//===----------------------------------------------------------------------===//
// CFGConversion
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class CFGConversionBase : public ::mlir::OperationPass<::mlir::FuncOp> {
public:
  using Base = CFGConversionBase;

  CFGConversionBase() : ::mlir::OperationPass<::mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  CFGConversionBase(const CFGConversionBase &other) : ::mlir::OperationPass<::mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("cfg-conversion");
  }
  ::llvm::StringRef getArgument() const override { return "cfg-conversion"; }

  ::llvm::StringRef getDescription() const override { return "Convert FIR structured control flow ops to CFG ops."; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("CFGConversion");
  }
  ::llvm::StringRef getName() const override { return "CFGConversion"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  registry.insert<mlir::StandardOpsDialect>();

  }

protected:
  ::mlir::Pass::Option<bool> forceLoopToExecuteOnce{*this, "always-execute-loop-body", ::llvm::cl::desc("force the body of a loop to execute at least once"), ::llvm::cl::init(false)};
};

//===----------------------------------------------------------------------===//
// CharacterConversion
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class CharacterConversionBase : public ::mlir::OperationPass<> {
public:
  using Base = CharacterConversionBase;

  CharacterConversionBase() : ::mlir::OperationPass<>(::mlir::TypeID::get<DerivedT>()) {}
  CharacterConversionBase(const CharacterConversionBase &other) : ::mlir::OperationPass<>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("character-conversion");
  }
  ::llvm::StringRef getArgument() const override { return "character-conversion"; }

  ::llvm::StringRef getDescription() const override { return "Convert CHARACTER entities with different KINDs"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("CharacterConversion");
  }
  ::llvm::StringRef getName() const override { return "CharacterConversion"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  }

protected:
  ::mlir::Pass::Option<std::string> useRuntimeCalls{*this, "use-runtime-calls", ::llvm::cl::desc("Generate runtime calls to a named set of conversion routines. By default, the conversions may produce unexpected results."), ::llvm::cl::init(std::string{})};
};

//===----------------------------------------------------------------------===//
// ExternalNameConversion
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class ExternalNameConversionBase : public ::mlir::OperationPass<mlir::ModuleOp> {
public:
  using Base = ExternalNameConversionBase;

  ExternalNameConversionBase() : ::mlir::OperationPass<mlir::ModuleOp>(::mlir::TypeID::get<DerivedT>()) {}
  ExternalNameConversionBase(const ExternalNameConversionBase &other) : ::mlir::OperationPass<mlir::ModuleOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("external-name-interop");
  }
  ::llvm::StringRef getArgument() const override { return "external-name-interop"; }

  ::llvm::StringRef getDescription() const override { return "Convert name for external interoperability"; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("ExternalNameConversion");
  }
  ::llvm::StringRef getName() const override { return "ExternalNameConversion"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  }

protected:
};

//===----------------------------------------------------------------------===//
// MemRefDataFlowOpt
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class MemRefDataFlowOptBase : public ::mlir::OperationPass<::mlir::FuncOp> {
public:
  using Base = MemRefDataFlowOptBase;

  MemRefDataFlowOptBase() : ::mlir::OperationPass<::mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  MemRefDataFlowOptBase(const MemRefDataFlowOptBase &other) : ::mlir::OperationPass<::mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("fir-memref-dataflow-opt");
  }
  ::llvm::StringRef getArgument() const override { return "fir-memref-dataflow-opt"; }

  ::llvm::StringRef getDescription() const override { return "Perform store/load forwarding and potentially removing dead stores."; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("MemRefDataFlowOpt");
  }
  ::llvm::StringRef getName() const override { return "MemRefDataFlowOpt"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  registry.insert<mlir::StandardOpsDialect>();

  }

protected:
};

//===----------------------------------------------------------------------===//
// MemoryAllocationOpt
//===----------------------------------------------------------------------===//

template <typename DerivedT>
class MemoryAllocationOptBase : public ::mlir::OperationPass<mlir::FuncOp> {
public:
  using Base = MemoryAllocationOptBase;

  MemoryAllocationOptBase() : ::mlir::OperationPass<mlir::FuncOp>(::mlir::TypeID::get<DerivedT>()) {}
  MemoryAllocationOptBase(const MemoryAllocationOptBase &other) : ::mlir::OperationPass<mlir::FuncOp>(other) {}

  /// Returns the command-line argument attached to this pass.
  static constexpr ::llvm::StringLiteral getArgumentName() {
    return ::llvm::StringLiteral("memory-allocation-opt");
  }
  ::llvm::StringRef getArgument() const override { return "memory-allocation-opt"; }

  ::llvm::StringRef getDescription() const override { return "Convert stack to heap allocations and vice versa."; }

  /// Returns the derived pass name.
  static constexpr ::llvm::StringLiteral getPassName() {
    return ::llvm::StringLiteral("MemoryAllocationOpt");
  }
  ::llvm::StringRef getName() const override { return "MemoryAllocationOpt"; }

  /// Support isa/dyn_cast functionality for the derived pass class.
  static bool classof(const ::mlir::Pass *pass) {
    return pass->getTypeID() == ::mlir::TypeID::get<DerivedT>();
  }

  /// A clone method to create a copy of this pass.
  std::unique_ptr<::mlir::Pass> clonePass() const override {
    return std::make_unique<DerivedT>(*static_cast<const DerivedT *>(this));
  }

  /// Return the dialect that must be loaded in the context before this pass.
  void getDependentDialects(::mlir::DialectRegistry &registry) const override {
    
  registry.insert<fir::FIROpsDialect>();

  }

protected:
  ::mlir::Pass::Option<bool> dynamicArrayOnHeap{*this, "dynamic-array-on-heap", ::llvm::cl::desc("Allocate all arrays with runtime determined size on heap."), ::llvm::cl::init(false)};
  ::mlir::Pass::Option<std::size_t> maxStackArraySize{*this, "maximum-array-alloc-size", ::llvm::cl::desc("Set maximum number of elements of an array allocated on the stack."), ::llvm::cl::init(~static_cast<std::size_t>(0))};
};
#undef GEN_PASS_CLASSES
#endif // GEN_PASS_CLASSES
#ifdef GEN_PASS_REGISTRATION

//===----------------------------------------------------------------------===//
// AbstractResultOpt Registration
//===----------------------------------------------------------------------===//

inline void registerAbstractResultOptPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createAbstractResultOptPass();
  });
}

//===----------------------------------------------------------------------===//
// AffineDialectDemotion Registration
//===----------------------------------------------------------------------===//

inline void registerAffineDialectDemotionPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createAffineDemotionPass();
  });
}

//===----------------------------------------------------------------------===//
// AffineDialectPromotion Registration
//===----------------------------------------------------------------------===//

inline void registerAffineDialectPromotionPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createPromoteToAffinePass();
  });
}

//===----------------------------------------------------------------------===//
// ArrayValueCopy Registration
//===----------------------------------------------------------------------===//

inline void registerArrayValueCopyPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createArrayValueCopyPass();
  });
}

//===----------------------------------------------------------------------===//
// CFGConversion Registration
//===----------------------------------------------------------------------===//

inline void registerCFGConversionPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createFirToCfgPass();
  });
}

//===----------------------------------------------------------------------===//
// CharacterConversion Registration
//===----------------------------------------------------------------------===//

inline void registerCharacterConversionPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createCharacterConversionPass();
  });
}

//===----------------------------------------------------------------------===//
// ExternalNameConversion Registration
//===----------------------------------------------------------------------===//

inline void registerExternalNameConversionPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createExternalNameConversionPass();
  });
}

//===----------------------------------------------------------------------===//
// MemRefDataFlowOpt Registration
//===----------------------------------------------------------------------===//

inline void registerMemRefDataFlowOptPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createMemDataFlowOptPass();
  });
}

//===----------------------------------------------------------------------===//
// MemoryAllocationOpt Registration
//===----------------------------------------------------------------------===//

inline void registerMemoryAllocationOptPass() {
  ::mlir::registerPass([]() -> std::unique_ptr<::mlir::Pass> {
    return ::fir::createMemoryAllocationPass();
  });
}

//===----------------------------------------------------------------------===//
// OptTransform Registration
//===----------------------------------------------------------------------===//

inline void registerOptTransformPasses() {
  registerAbstractResultOptPass();
  registerAffineDialectDemotionPass();
  registerAffineDialectPromotionPass();
  registerArrayValueCopyPass();
  registerCFGConversionPass();
  registerCharacterConversionPass();
  registerExternalNameConversionPass();
  registerMemRefDataFlowOptPass();
  registerMemoryAllocationOptPass();
}
#undef GEN_PASS_REGISTRATION
#endif // GEN_PASS_REGISTRATION
