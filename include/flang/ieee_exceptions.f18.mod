﻿!mod$ v1 sum:3c9832d934639a12
module ieee_exceptions
type::ieee_flag_type
integer(1),private::flag=0_1
end type
type(ieee_flag_type),parameter::ieee_invalid=ieee_flag_type(flag=1_1)
type(ieee_flag_type),parameter::ieee_overflow=ieee_flag_type(flag=2_1)
type(ieee_flag_type),parameter::ieee_divide_by_zero=ieee_flag_type(flag=4_1)
type(ieee_flag_type),parameter::ieee_underflow=ieee_flag_type(flag=8_1)
type(ieee_flag_type),parameter::ieee_inexact=ieee_flag_type(flag=16_1)
type(ieee_flag_type),parameter::ieee_denorm=ieee_flag_type(flag=32_1)
type(ieee_flag_type),parameter::ieee_usual(1_8:*)=[ieee_flag_type::ieee_flag_type(flag=2_1),ieee_flag_type(flag=4_1),ieee_flag_type(flag=1_1)]
type(ieee_flag_type),parameter::ieee_all(1_8:*)=[ieee_flag_type::ieee_flag_type(flag=2_1),ieee_flag_type(flag=4_1),ieee_flag_type(flag=1_1),ieee_flag_type(flag=8_1),ieee_flag_type(flag=16_1),ieee_flag_type(flag=32_1)]
type::ieee_modes_type
end type
type::ieee_status_type
end type
interface ieee_support_flag
procedure::ieee_support_flag
procedure::ieee_support_flag_2
procedure::ieee_support_flag_3
procedure::ieee_support_flag_4
procedure::ieee_support_flag_8
procedure::ieee_support_flag_10
procedure::ieee_support_flag_16
end interface
private::ieee_support_flag_2
private::ieee_support_flag_3
private::ieee_support_flag_4
private::ieee_support_flag_8
private::ieee_support_flag_10
private::ieee_support_flag_16
contains
pure function ieee_support_flag(flag)
type(ieee_flag_type),intent(in)::flag
logical(4)::ieee_support_flag
end
elemental subroutine ieee_get_flag(flag,flag_value)
type(ieee_flag_type),intent(in)::flag
logical(4),intent(out)::flag_value
end
elemental subroutine ieee_get_halting_mode(flag,halting)
type(ieee_flag_type),intent(in)::flag
logical(4),intent(out)::halting
end
subroutine ieee_get_modes(modes)
type(ieee_modes_type),intent(out)::modes
end
subroutine ieee_get_status(status)
type(ieee_status_type),intent(out)::status
end
pure subroutine ieee_set_flag(flag,flag_value)
type(ieee_flag_type),intent(in)::flag
logical(4),intent(in)::flag_value
end
pure subroutine ieee_set_halting_mode(flag,halting)
type(ieee_flag_type),intent(in)::flag
logical(4),intent(in)::halting
end
subroutine ieee_set_modes(modes)
type(ieee_modes_type),intent(in)::modes
end
subroutine ieee_set_status(status)
type(ieee_status_type),intent(in)::status
end
pure function ieee_support_flag_2(flag,x)
type(ieee_flag_type),intent(in)::flag
real(2),intent(in)::x(..)
logical(4)::ieee_support_flag_2
end
pure function ieee_support_flag_3(flag,x)
type(ieee_flag_type),intent(in)::flag
real(3),intent(in)::x(..)
logical(4)::ieee_support_flag_3
end
pure function ieee_support_flag_4(flag,x)
type(ieee_flag_type),intent(in)::flag
real(4),intent(in)::x(..)
logical(4)::ieee_support_flag_4
end
pure function ieee_support_flag_8(flag,x)
type(ieee_flag_type),intent(in)::flag
real(8),intent(in)::x(..)
logical(4)::ieee_support_flag_8
end
pure function ieee_support_flag_10(flag,x)
type(ieee_flag_type),intent(in)::flag
real(10),intent(in)::x(..)
logical(4)::ieee_support_flag_10
end
pure function ieee_support_flag_16(flag,x)
type(ieee_flag_type),intent(in)::flag
real(16),intent(in)::x(..)
logical(4)::ieee_support_flag_16
end
pure function ieee_support_halting(flag)
type(ieee_flag_type),intent(in)::flag
logical(4)::ieee_support_halting
end
end
